﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour {

	public Texture2D textCrosshair;
	// Maquina d'estats
	public enum StateTypeGame{
		esperarPlay,
		assignAndShow,
		waitToken1,
		waitToken2,
		Check,
		FinishGame,
		FailTokens,
		CorrectTokens,
		ClueLife
	}
	public int numCorrectTokens = 0;
	public CubeScript scriptSelected_1;
	public CubeScript scriptSelected_2;

	//instanciem l'statetypegame
	public StateTypeGame state;
	public CubeScript[] cube;
	public Sprite[] images;
	public Sprite[] vides;
	int[] pictures;
	public GameObject camera;
	public GameObject btnPlay;
	public GameObject panel;
	//public Vida[] vida;


	int numFitxes = 24;
	bool tokenAssignat;

	//variables de la maquina 
	bool isFirstTime;
	float timer; 
	public float tempsGirar = 2.0F;

	//public dsprites crear vincle (12 imatges), public sprite sprites; 

	// Use this for initialization
	void Start () {
		isFirstTime = true;
		foreach(CubeScript _script in cube)
		{
			_script.Selected(false);
		}
	
	}

	// Update is called once per frame
	void Update () {
	

		Debug.Log (state);
		updateStateMachine ();

		RaycastHit hit;
		float distanceToGround = 0;


		foreach(CubeScript _script in cube)
		{
			_script.Selected(false);
		}

		if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, 100.0F)) {
			if (hit.collider.gameObject.tag == "Token")
			{
				if (state == StateTypeGame.waitToken1)
				{
					CubeScript scriptSelected = hit.collider.gameObject.GetComponent<CubeScript>();
					scriptSelected.Selected(true);
					Debug.Log ("detectant");

					if(Input.GetKeyDown(KeyCode.Space))
					{
						scriptSelected_1 = scriptSelected;
						scriptSelected.ShowCube();
						ChangeState(StateTypeGame.waitToken2);
					}
				}
				else if (state == StateTypeGame.waitToken2)
				{
					CubeScript scriptSelected = hit.collider.gameObject.GetComponent<CubeScript>();
					scriptSelected.Selected(true);
					Debug.Log ("detectant");
					if(Input.GetKeyDown(KeyCode.Space))
					{
						scriptSelected_2 = scriptSelected;
						scriptSelected.ShowCube();
						ChangeState(StateTypeGame.Check);
					}
				}
			}
		}



		if (Input.GetKeyDown(KeyCode.G))
		{

			for (int i = 0; i < 24; i++) {
			cube[i].ShowCube();
			}
			Invoke("AmagaCube", tempsGirar);
		}

		/*if (Input.GetKeyDown(KeyCode.R)){
			RepartirImatges();
		}*/

		/*
		if (Input.GetKey (KeyCode.S)) {

			cube[1].Selected (true);


		} else {

			cube[1].Selected (false);

		}
		*/
	}
	
	public void GirarTokens(){
		{
			
			for (int i = 0; i < 24; i++) {
				cube[i].ShowCube();
			}
			Invoke("AmagaCube", tempsGirar);
		}
	}

	public void AmagaCube()
	{
		for (int i = 0; i < 24; i++) {
			cube [i].HideCube ();
		}

	}


	public void RepartirImatges(){
		pictures = new int[cube.Length/2];
		for (int i = 0; i < numFitxes; i++) {
			tokenAssignat = false;
			//Debug.Log("Token "+i);
			while (tokenAssignat == false){
				int maxIndex = (cube.Length/2);
				int foto = Random.Range(0 , maxIndex);
				//Debug.Log("Foto "+foto);
				
				if (pictures[foto] < 2){

					cube[i].image = foto;
					cube[i].SetPicture(images[foto]);
					//cube[i].gat.sprite = gats_[foto];
					tokenAssignat = true;
					pictures[foto]++;
					Debug.Log("Foto "+foto+" assignada al token "+i);
				}
			}
		}
	}

	void ChangeState(StateTypeGame _state)
	{
		state = _state;
		timer = 0.0f;
		isFirstTime = true;
	}

//Maquina d'estats

	/*void OnClick()
	{

			panel.gameObject.SetActive(false);
			ChangeState(StateTypeGame.assignAndShow);
			//btnPlay = false;


	}*/


	void updateStateMachine ()
	{
		timer += Time.deltaTime;
		
		switch (state) 
		{
			case StateTypeGame.esperarPlay:
				if (isFirstTime)
				{
					Debug.Log("apretarPlay");
					isFirstTime = false;

				}
				else
				{

				//if (Input.GetButton("Play"))
				if (Input.GetKeyDown(KeyCode.P))
					{
					    panel.gameObject.SetActive(false);
						ChangeState(StateTypeGame.assignAndShow);
						//btnPlay = false;
					}

				}
			break;
		case StateTypeGame.assignAndShow:
			if (isFirstTime)
			{

				Debug.Log ("Assignant Fotos");
				RepartirImatges();

				GirarTokens();
				isFirstTime = false;
			}
			else
			{
				if (timer > tempsGirar)
				{
					ChangeState(StateTypeGame.waitToken1);
				}
			}
			
			break;
		case StateTypeGame.waitToken1:
			if (isFirstTime)
			{
				Debug.Log("waitToken1");
				isFirstTime = false;
			}
			else
			{
				//face.SetFace(CP_Face.CP_TypeFaces.Normal);
			}
			
			break;
		case StateTypeGame.waitToken2:
			if (isFirstTime)
			{
				Debug.Log("waitToken2");
				isFirstTime = false;
			}
			else
			{
				
			}
			
			break;
		case StateTypeGame.Check:
			if (isFirstTime)
			{
				Debug.Log("Check");
				isFirstTime = false;
				if (scriptSelected_1.image == scriptSelected_2.image)
				{
					Debug.Log("Son iguals");
					numCorrectTokens+=2;


					if (numCorrectTokens == cube.Length)
					{
						ChangeState(StateTypeGame.FinishGame);
					}else
					{
						ChangeState(StateTypeGame.CorrectTokens);
					}
				}
				else{
					ChangeState(StateTypeGame.FailTokens);
				}
			}
			else
			{
				//comprovar();
			}
			break;
		case StateTypeGame.CorrectTokens:			
			if (isFirstTime)
			{
				Debug.Log("CorrectTokens");
				isFirstTime = false;
				scriptSelected_1.gameObject.SetActive(false);
				scriptSelected_2.gameObject.SetActive(false);

				ChangeState(StateTypeGame.waitToken1);
			}
			else
			{
				if (timer > 1f)
				{
					/*destruir();
					if (IsFinishGame())
					{
						ChangeState(StateTypeGame.FinishGame);
					}
					else{
						ChangeState(StateTypeGame.waitToken1);
					}*/
					
				}
			}
			break;
		case StateTypeGame.FailTokens:
			if (isFirstTime)
			{
				Debug.Log("FailTokens");
				isFirstTime = false;
			}
			else
			{
				if (timer > 2f)
				{
					scriptSelected_1.HideCube();
					scriptSelected_2.HideCube();
					ChangeState(StateTypeGame.waitToken1);
				}
				
			}
			break;
		case StateTypeGame.FinishGame:
			if (isFirstTime)
			{
				Debug.Log("FinishGame");
				isFirstTime = false;
				
				/*StartCoroutine(faces());
				updateScore();*/
			}
			else
			{
				
			}
			break;
			
		case StateTypeGame.ClueLife:
			if (isFirstTime)
			{
				Debug.Log("ClueLife");
				isFirstTime = false;
			}
			else
			{
				if (timer > 2f)
				{
					
					//ChangeState(StateTypeGame.waitToken1);
				}
			}
			break;
		default:
			//Debug.LogError ("updateStateMachine: Unvalid state");
			break;
		}
	}

	void OnGUI ()
	{
		float w = 50.0f;
		float h = 50.0f;
		float posX = Screen.width * 0.5f - w * 0.5f;
		float posY = Screen.height * 0.5f - h * 0.5f;


		GUI.DrawTexture(new Rect(posX, posY, w, h), textCrosshair);


	}
}




