﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class CubeScript : MonoBehaviour {
	public GameObject punter;
	public SpriteRenderer spriteRenderer;
	public int image;
	public Image cara;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void ShowCube()
	{
		gameObject.transform.Rotate(0.0f, 180.0f, 0.0f);

	}

	public void HideCube()
	{
		gameObject.transform.Rotate(0.0f, 180.0f, 0.0f);
		
	}


	public void Selected (bool sel)
	{
		punter.SetActive (sel);
	}

	public void SetPicture (Sprite sprite)
	{
		spriteRenderer.sprite = sprite;
	}
}
